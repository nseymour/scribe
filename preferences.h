#ifndef PREFERENCES_H
#define PREFERENCES_H

#include <iostream>
#include <vector>
#include "ijsonserializable.h"
#include "jsonserializer.h"

class Preferences : public IJsonSerializable
{
public:
    Preferences();
    virtual void Serialize( Json::Value& root );
    virtual void Deserialize( Json::Value& root);

    std::vector<std::string> LangPaths;
};

#endif // PREFERENCES_H
