#ifndef READER_H
#define READER_H

#include <QMainWindow>
#include <QThread>
#include <QString>
#include <iostream>
#include "dictloader.h"

namespace Ui {
class Reader;
}

class Reader : public QMainWindow
{
    Q_OBJECT

public:
    explicit Reader(Language* lang, std::string path, QWidget *parent = 0);
    ~Reader();

private slots:
    void enableDict();

    void on_txt_reader_selectionChanged();

    void on_btn_create_clicked();

    void on_list_trans_currentTextChanged(const QString &currentText);

private:
    Ui::Reader *ui;
    QThread workerThread;
    DictLoader *loader;
    Language* language;

    QString selectedtext;

    void RefreshCards();

signals:
    void LoadDict(Language* lang, bool reverse = false);
};

#endif // READER_H
