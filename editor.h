#ifndef EDITOR_H
#define EDITOR_H

#include <QMainWindow>
#include <iostream>
#include <QString>
#include <QStringList>
#include <QThread>
#include <QFileDialog>
#include <fstream>
#include <algorithm>
#include "language.h"
#include "dictloader.h"

namespace Ui {
class Editor;
}

class DictLoader;

class Editor : public QMainWindow
{
    Q_OBJECT

public:
    class Prefs
    {
    public:
        int MinSentences = -1;

        Prefs(int sent = -1) : MinSentences(sent) {}
    };

    explicit Editor(Language* lang, Prefs *preferences, std::string prompt = "PROMPT", QWidget *parent = 0);
    ~Editor();

private slots:
    void on_btn_search_clicked();
    void enableDict();

    void SaveAs();

    void on_txt_editor_textChanged();

private:
    Ui::Editor *ui;
    Language *language;
    QThread workerThread;
    DictLoader *loader;
    Prefs* prefs;

signals:
    void LoadDict(Language* lang, bool reverse = false);
};

#endif // EDITOR_H
