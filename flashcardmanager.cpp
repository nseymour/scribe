#include "flashcardmanager.h"

FlashcardManager::FlashcardManager()
{

}

void FlashcardManager::Serialize( Json::Value& root )
{
    for(int i = 0; i < Flashcards.size(); i++)
    {
        root["flashcards"][i]["front"] = Flashcards[i]->Front;
        root["flashcards"][i]["back"] = Flashcards[i]->Back;
    }
}

void FlashcardManager::Deserialize( Json::Value& root)
{
    Json::Value cards = root.get("flashcards", 0);

    for(int i = 0; i < cards.size(); i++)
    {
        Flashcards.push_back(new Flashcard(cards[i].get("front", "ERR").asString(), cards[i].get("back", "ERR").asString()));
    }
}


