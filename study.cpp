#include "study.h"
#include "ui_study.h"

Study::Study(Language* lang, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Study)
{
    ui->setupUi(this);

    language = lang;

    Refresh();
}

Study::~Study()
{
    delete ui;
}

void Study::Refresh()
{
    if(Front)
    {
        ui->lbl_card->setText(QString::fromStdString(language->FManager->Flashcards[index]->Front));
    } else
    {
        ui->lbl_card->setText(QString::fromStdString(language->FManager->Flashcards[index]->Back));
    }
}

void Study::on_btn_prev_clicked()
{
    if(index <= 0)
    {
        index = language->FManager->Flashcards.size() - 1;
    } else
    {
        index--;
    }

    Front = true;

    Refresh();
}

void Study::on_btn_next_clicked()
{
    if(index >= language->FManager->Flashcards.size() - 1)
    {
        index = 0;
    } else
    {
        index++;
    }

    Front = true;

    Refresh();
}

void Study::on_btn_flip_clicked()
{
    Front = !Front;
    Refresh();
}
