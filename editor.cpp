#include "editor.h"
#include "ui_editor.h"

Editor::Editor(Language* lang, Prefs *preferences, std::string prompt, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Editor)
{
    ui->setupUi(this);

    ui->lbl_prompt->setText(QString::fromStdString(prompt));

    language = lang;
    prefs = preferences;

    // Start new thread for loading dict
    loader = new DictLoader;
    loader->moveToThread(&workerThread);
    connect(this, &Editor::LoadDict, loader, &DictLoader::doWork);
    connect(loader, &DictLoader::enableDict, this, &Editor::enableDict);
    workerThread.start();

    LoadDict(language);

    connect(ui->btn_save, SIGNAL(triggered()), this, SLOT(SaveAs()));

    if(prefs->MinSentences != -1) ui->lbl_minsent->setText(QString::number(prefs->MinSentences));
}

Editor::~Editor()
{
    delete ui;
}

void Editor::on_btn_search_clicked()
{
    std::vector<std::string> results = language->Lookup(ui->txt_search->text().toStdString(), ui->chk_fullmatch->isChecked());

    if(results.size() == 0) results.push_back("No results found...");

    QStringList resList;

    for(int i = 0; i < results.size(); i++)
    {
        resList.append(QString::fromStdString(results[i]));
    }

    ui->list_results->clear();
    ui->list_results->addItems(resList);
}

void Editor::enableDict()
{
    ui->btn_search->setEnabled(true);
    ui->txt_search->setEnabled(true);
    ui->list_results->setEnabled(true);
    ui->chk_fullmatch->setEnabled(true);

    ui->lbl_status->setText(QString::fromStdString("Powered by Freedict"));

    delete loader;
}

void Editor::SaveAs()
{
    QString filePath = QFileDialog::getOpenFileName(this, tr("Save Writing Prompt"), "C:\\", tr("Text Files (*.txt)"));

    std::ofstream fileStream;
    fileStream.open(filePath.toStdString());
    fileStream << ui->txt_editor->toPlainText().toStdString();

    fileStream.close();
}

void Editor::on_txt_editor_textChanged()
{
    if(ui->txt_editor->toPlainText().toStdString().find(". ") >= prefs->MinSentences)
    {
        ui->lbl_minsent->setStyleSheet(QString::fromStdString("color: green;"));
    }
    else
    {
        ui->lbl_minsent->setStyleSheet(QString::fromStdString("color: red;"));
    }
}
