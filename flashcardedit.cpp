#include "flashcardedit.h"
#include "ui_flashcardedit.h"

FlashcardEdit::FlashcardEdit(FlashcardManager::Flashcard *card, QWidget *parent) :
    QDialog(parent),
    flashcard(card),
    ui(new Ui::FlashcardEdit)
{
    ui->setupUi(this);

    ui->txt_front->setText(QString::fromStdString(flashcard->Front));
    ui->txt_back->setText(QString::fromStdString(flashcard->Back));
}

FlashcardEdit::~FlashcardEdit()
{
    delete ui;
}

void FlashcardEdit::on_btn_controls_accepted()
{
    flashcard->Front = ui->txt_front->text().toStdString();
    flashcard->Back = ui->txt_back->text().toStdString();
}

void FlashcardEdit::on_btn_controls_rejected()
{
}
