#ifndef JSONSERIALIZER_H
#define JSONSERIALIZER_H

#include "ijsonserializable.h"
#include <iostream>
#include <fstream>
#include "json/json.h"

class JsonSerializer
{
public:
    static bool Serialize( IJsonSerializable* pObj, std::string& output );
    static bool Deserialize( IJsonSerializable* pObj, std::string& input );
    static bool DeserializeFromFilepath(IJsonSerializable* pObj, std::string path);
    static bool SerializeToFilepath(IJsonSerializable* pObj, std::string path);

private:
    JsonSerializer( void ) {}
};

#endif // JSONSERIALIZER_H
