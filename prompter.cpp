#include "prompter.h"
#include "langwriter.h"

Prompter::Prompter()
{
    if (!JsonSerializer::DeserializeFromFilepath(this, LangWriter::GetFullPath("DATA/prompter.json")))
    {
        Patterns.push_back("Deserialization FAILED! Check filepath!");
    }
}

void Prompter::Serialize( Json::Value& root )
{
}

void Prompter::JsonArrayToVector( Json::Value& array, std::vector<std::string> *vect )
{
    for(int i = 0; i < array.size(); i++)
    {
        vect->push_back(array[i].asString());
    }
}

void Prompter::Deserialize( Json::Value& root )
{
    Json::Value prompter_data = root.get("prompter_data", 0);

    Json::Value patterns = prompter_data.get("patterns", 0);
    this->JsonArrayToVector(patterns, &Patterns);

    Json::Value qphrases = prompter_data.get("q_phrases", 0);
    this->JsonArrayToVector(qphrases, &QPhrases);

    Json::Value adjectives = prompter_data.get("adjectives", 0);
    this->JsonArrayToVector(adjectives, &Adjectives);

    Json::Value nouns = prompter_data.get("nouns", 0);
    this->JsonArrayToVector(nouns, &Nouns);

    Json::Value possessives = prompter_data.get("possessives", 0);
    this->JsonArrayToVector(possessives, &Possessives);

    Json::Value imperatives = prompter_data.get("imperatives", 0);
    this->JsonArrayToVector(imperatives, &Imperatives);

    Json::Value prepositions = prompter_data.get("prepositions", 0);
    this->JsonArrayToVector(prepositions, &Prepositions);
}

std::string Prompter::StringPrompt()
{
    std::string prompt;

    // RANDOM NUMBERS!!
    srand(time(0));
    long long seed = rand();

    std::default_random_engine eng{static_cast<unsigned int>(std::chrono::high_resolution_clock::now().time_since_epoch().count())};

    std::uniform_int_distribution<> r_patterns(0, Patterns.size() - 1);
    std::uniform_int_distribution<> r_qphrases(0, QPhrases.size() - 1);
    std::uniform_int_distribution<> r_adjectives(0, Adjectives.size() - 1);
    std::uniform_int_distribution<> r_nouns(0, Nouns.size() - 1);
    std::uniform_int_distribution<> r_possessives(0, Possessives.size() - 1);
    std::uniform_int_distribution<> r_imperatives(0, Imperatives.size() - 1);
    std::uniform_int_distribution<> r_prepositions(0, Prepositions.size() - 1);

    // Grab a random pattern and split it
    char* cstr=const_cast<char*>(Patterns[r_patterns(eng)].c_str());
    char* current;
    std::vector<std::string> pattern_vector;
    char* sep = (char*)"+";
    current=strtok(cstr,sep);
    while(current!=NULL){
        pattern_vector.push_back(current);
        current=strtok(NULL,sep);
    }

    // Where the magic happens...
    for(int i = 0; i < pattern_vector.size(); i++)
    {
        if(pattern_vector[i] == "QPHRASE")
        {
            prompt.append(QPhrases[r_qphrases(eng)]);
        } else
        if(pattern_vector[i] == "ADJECTIVE")
        {
            prompt.append(Adjectives[r_adjectives(eng)]);
        } else
        if(pattern_vector[i] == "NOUN")
        {
            prompt.append(Nouns[r_nouns(eng)]);
        } else
        if(pattern_vector[i] == "POSSESSIVE")
        {
            prompt.append(Possessives[r_possessives(eng)]);
        } else
        if(pattern_vector[i] == "IMPERATIVE")
        {
            prompt.append(Imperatives[r_imperatives(eng)]);
        } else
        if(pattern_vector[i] == "PREPOSITION")
        {
            prompt.append(Prepositions[r_prepositions(eng)]);
        } else
        if(pattern_vector[i] == "ARTICLE")
        {
            prompt.append("a");
        } else prompt.append(pattern_vector[i]);

        prompt.append(" ");
    }

    return prompt;
}

























