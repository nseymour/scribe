#ifndef LANGWRITER_H
#define LANGWRITER_H

#include <QMainWindow>
#include <QMessageBox>
#include <QListWidgetItem>
#include <QStringList>
#include <vector>
#include <iostream>
#include "json/json.h"
#include "language.h"
#include "jsonserializer.h"
#include "editor.h"
#include "reader.h"
#include "study.h"
#include "preferences.h"
#include "flashcardedit.h"

#define WRITING_GENERATEDPROMPT 0
#define WRITING_FREEWRITE 1

#define READING_FROMFILE 0

#define VERSION "Alpha 0.1"

namespace Ui {
class LangWriter;
}

class LangWriter : public QMainWindow
{
    Q_OBJECT

public:
    explicit LangWriter(QWidget *parent = 0);
    ~LangWriter();

    static std::string GetFullPath(std::string relative);

private slots:
    void on_radio_reading_clicked();
    void on_radio_writing_clicked();

    void on_btn_start_clicked();
    void on_list_langs_currentRowChanged(int currentRow);

    void on_chk_min_toggled(bool checked);

    void VersionInfo();
    void Libs();

    void on_btn_refresh_clicked();

    void on_btn_study_clicked();

    void on_btn_add_clicked();

    void on_pushButton_clicked();

    void on_btn_delete_clicked();

private:
    Ui::LangWriter *ui;

    QStringList excercises_writing;
    QStringList excercises_reading;

    Preferences *prefs;

    std::vector<Language*> Langs;
    int selectedlang = -1;

    Editor *editor;
    Reader *reader;
    Study *study;

    void RefreshCards();
};

#endif // LANGWRITER_H
