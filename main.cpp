#include "langwriter.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    LangWriter w;
    w.show();

    return a.exec();
}
