#include "reader.h"
#include "ui_reader.h"

Reader::Reader(Language* lang, std::string path, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Reader)
{
    ui->setupUi(this);

    // Open file
    std::string line;
    std::string output;
    std::ifstream file(path);
    if(file.is_open())
    {
        while(std::getline(file, line))
        {
            output.append(line + "\n");
        }
        file.close();
    }

    ui->txt_reader->insertPlainText(QString::fromStdString(output));

    language = lang;

    // Start new thread for loading dict
    loader = new DictLoader;
    loader->moveToThread(&workerThread);
    connect(this, &Reader::LoadDict, loader, &DictLoader::doWork);
    connect(loader, &DictLoader::enableDict, this, &Reader::enableDict);
    workerThread.start();

    LoadDict(language, true);

    RefreshCards();
}

Reader::~Reader()
{
    delete ui;
}

void Reader::enableDict()
{
    ui->lbl_status->setText(QString::fromStdString("Powered by Freedict"));
}

void Reader::on_txt_reader_selectionChanged()
{
    selectedtext = ui->txt_reader->textCursor().selectedText();
    ui->lbl_selected->setText(selectedtext);
    ui->txt_front->setText(selectedtext);

    std::vector<std::string> results = language->Lookup(selectedtext.toStdString(), ui->chk_fullmatch->isChecked());

    if(results.size() == 0) results.push_back("No results found...");

    QStringList resList;

    for(int i = 0; i < results.size(); i++)
    {
        resList.append(QString::fromStdString(results[i]));
    }

    ui->list_trans->clear();
    ui->list_trans->addItems(resList);
}

void Reader::RefreshCards()
{
    ui->list_cards->clear();
    for(int i = 0; i < language->FManager->Flashcards.size(); i++)
    {
        ui->list_cards->addItem(QString::fromStdString(language->FManager->Flashcards[i]->asString()));
    }
}

void Reader::on_btn_create_clicked()
{
    language->FManager->Flashcards.push_back(new FlashcardManager::Flashcard(ui->txt_front->text().toStdString(), ui->txt_back->text().toStdString()));

    ui->txt_front->clear();
    ui->txt_back->clear();

    RefreshCards();
}

void Reader::on_list_trans_currentTextChanged(const QString &currentText)
{
    ui->txt_back->setText(currentText);
}
