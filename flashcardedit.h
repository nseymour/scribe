#ifndef FLASHCARDEDIT_H
#define FLASHCARDEDIT_H

#include <QDialog>
#include "flashcardmanager.h"

namespace Ui {
class FlashcardEdit;
}

class FlashcardEdit : public QDialog
{
    Q_OBJECT

public:
    explicit FlashcardEdit(FlashcardManager::Flashcard *card, QWidget *parent = 0);
    ~FlashcardEdit();

    FlashcardManager::Flashcard *flashcard;

private slots:
    void on_btn_controls_accepted();

    void on_btn_controls_rejected();

private:
    Ui::FlashcardEdit *ui;
};

#endif // FLASHCARDEDIT_H
