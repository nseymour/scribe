#include "langwriter.h"
#include "ui_langwriter.h"
#include "prompter.h"

LangWriter::LangWriter(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LangWriter)
{
    ui->setupUi(this);

    prefs = new Preferences();

    for(int i = 0; i < prefs->LangPaths.size(); i++)
    {
        Langs.push_back(new Language());

        JsonSerializer::DeserializeFromFilepath(Langs[i], prefs->LangPaths[i]);

        ui->list_langs->addItem(QString::fromStdString(Langs[i]->printName));
    }

    excercises_writing << "Generated Prompt" << "Freewrite";
    excercises_reading << "Local File";

    ui->list_excercise->addItems(excercises_writing);

    connect(ui->btn_version, SIGNAL(triggered()), this, SLOT(VersionInfo()));
    connect(ui->btn_libs, SIGNAL(triggered()), this, SLOT(Libs()));
}

LangWriter::~LangWriter()
{
    delete ui;
    delete prefs;

    for(int i = 0; i < Langs.size(); i++)
    {
        delete Langs[i];
    }
}

void LangWriter::on_radio_reading_clicked()
{
    ui->list_excercise->clear();
    ui->list_excercise->addItems(excercises_reading);
}

void LangWriter::on_radio_writing_clicked()
{
    ui->list_excercise->clear();
    ui->list_excercise->addItems(excercises_writing);
}

void LangWriter::on_btn_start_clicked()
{
    if(selectedlang != -1 && ui->list_excercise->currentRow() != -1)
    {
        if(ui->radio_writing->isChecked())
        {
            Prompter *prompter = new Prompter();

            Editor::Prefs *editprefs = new Editor::Prefs();
            if(ui->spin_msent->isEnabled()) editprefs->MinSentences = ui->spin_msent->value();

            switch(ui->list_excercise->currentRow())
            {
                case WRITING_GENERATEDPROMPT:
                {
                    std::string prompt = prompter->StringPrompt();
                    if(ui->chk_editor->isChecked())
                    {
                        editor = new Editor(Langs[selectedlang], editprefs, prompt);
                        editor->show();
                    } else
                    {
                        QMessageBox msg;
                        msg.setText(QString::fromStdString(prompt));
                        msg.exec();
                    }
                    break;
                }

                case WRITING_FREEWRITE:
                {
                    editor = new Editor(Langs[selectedlang], editprefs, "This is a freewrite session, write whatever comes to heart!");
                    editor->show();
                    break;
                }
            }

            delete prompter;
            delete editprefs;
        } else
        {
            switch(ui->list_excercise->currentRow())
            {
                case READING_FROMFILE:
                {
                    QString filePath = QFileDialog::getOpenFileName(this, tr("Open File"), "C:\\", tr("Text Files (*.txt)"));
                    reader = new Reader(Langs[selectedlang], filePath.toStdString());
                    reader->show();
                }
            }
        }
    } else
    {
        QMessageBox msg;
        msg.setText(QString::fromStdString("You must select both a language and an excercise before continuing!"));
        msg.exec();
    }
}

void LangWriter::on_list_langs_currentRowChanged(int currentRow)
{
    selectedlang = currentRow;

    RefreshCards();
}

void LangWriter::on_chk_min_toggled(bool checked)
{
    ui->spin_msent->setEnabled(checked);
}

void LangWriter::VersionInfo()
{
    QMessageBox msg;
    msg.setText(QString::fromUtf8(VERSION));
    msg.exec();
}

void LangWriter::Libs()
{
    QMessageBox msg;
    msg.setText(QString::fromStdString("This application uses QT, RapidXML, Freedict, and jsoncpp. Their respective liscenses and information can be found in the install directory."));
    msg.exec();
}

void LangWriter::RefreshCards()
{
    ui->list_cards->clear();
    for(int i = 0; i < Langs[selectedlang]->FManager->Flashcards.size(); i++)
    {
        ui->list_cards->addItem(QString::fromStdString(Langs[selectedlang]->FManager->Flashcards[i]->asString()));
    }
}

void LangWriter::on_btn_refresh_clicked()
{
    RefreshCards();
}

void LangWriter::on_btn_study_clicked()
{
    if(selectedlang != -1)
    {
        study = new Study(Langs[selectedlang]);
        study->show();
    }
}

std::string LangWriter::GetFullPath(std::string relative)
{
    return QCoreApplication::applicationDirPath().toStdString() + "/" + relative;
}

void LangWriter::on_btn_add_clicked()
{
    FlashcardManager::Flashcard *card = new FlashcardManager::Flashcard();
    FlashcardEdit *edit = new FlashcardEdit(card);
    edit->exec();

    if(card->Front != "") Langs[selectedlang]->FManager->Flashcards.push_back(card);

    delete edit;

    RefreshCards();
}

void LangWriter::on_pushButton_clicked()
{
    FlashcardEdit *edit = new FlashcardEdit(Langs[selectedlang]->FManager->Flashcards[ui->list_cards->currentRow()]);
    edit->exec();

    delete edit;

    RefreshCards();
}

void LangWriter::on_btn_delete_clicked()
{
    Langs[selectedlang]->FManager->Flashcards.erase(std::remove(Langs[selectedlang]->FManager->Flashcards.begin(),
                                                                Langs[selectedlang]->FManager->Flashcards.end(),
                                                                Langs[selectedlang]->FManager->Flashcards[ui->list_cards->currentRow()]),
                                                     Langs[selectedlang]->FManager->Flashcards.end());

    RefreshCards();
}
