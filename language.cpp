#include "language.h"
#include "langwriter.h"

Language::Language()
{
    FManager = new FlashcardManager();
}

Language::~Language( void ) {
    JsonSerializer::SerializeToFilepath(FManager, FlashcardPath);

    delete FManager;
}

void Language::Serialize( Json::Value& root )
{
}

void Language::Deserialize( Json::Value& root )
{
    Json::Value info = root.get("info", 0);

    DictPath = LangWriter::GetFullPath(info.get("path_dict", "").asString());
    DictReversePath = LangWriter::GetFullPath(info.get("path_r_dict", "").asString());

    printName = info.get("printName", "UNKNOWN").asString();

    FlashcardPath = LangWriter::GetFullPath(info.get("path_cards", 0).asString());

    JsonSerializer::DeserializeFromFilepath(FManager, FlashcardPath);
}

std::vector<std::string> Language::Lookup(std::string word, bool onlyfull)
{
    std::vector<std::string> results;

    std::string orth;
    std::string wordlower;
    for (int i = 0; i < Entries.size(); i++)
    {
        orth = Entries[i]->Orth;
        wordlower = word;
        std::transform(wordlower.begin(), wordlower.end(), wordlower.begin(), ::tolower);
        if(onlyfull) {
            if(orth == word || orth == wordlower) results.insert(std::end(results), std::begin(Entries[i]->Trans), std::end(Entries[i]->Trans));
        } else
        if(orth.find(word) != std::string::npos) results.insert(std::end(results), std::begin(Entries[i]->Trans), std::end(Entries[i]->Trans));
    }

    return results;
}

// This function needs a LOT of cleanup!
void Language::DictInit(bool reverse)
{
    std::string line;
    std::ifstream dictxml;

    if(!reverse) dictxml.open(DictPath);
    else dictxml.open(DictReversePath);

    Dict = "";
    if(dictxml.is_open())
    {
        while(std::getline(dictxml, line))
        {
            Dict.append(line + "\n");
        }
        dictxml.close();
    }

    Entries.clear();

    DictXML.parse<0>((char*)Dict.c_str());
    rapidxml::xml_node<> *body = DictXML.first_node()->first_node()->first_node();

    rapidxml::xml_node<> *currentry = body->first_node("entry");
    while (currentry = currentry->next_sibling("entry"))
    {
        std::string orth = currentry->first_node("form")->first_node("orth")->value();
        std::vector<std::string> trans;

        rapidxml::xml_node<> *cit = currentry->last_node("sense")->first_node("cit");
        if (cit != 0)
        {
            rapidxml::xml_node<> *quote = cit->first_node("quote");
            rapidxml::xml_node<> *gramGrp = quote->next_sibling("gramGrp");
            rapidxml::xml_node<> *gen;
            if(gramGrp != 0)
            {
                gen = gramGrp->first_node("gen");
                if(gen != 0) trans.push_back(std::string(quote->value()) + " {" + std::string(gen->value()) + "}");
                else trans.push_back(std::string(quote->value()));
            } else trans.push_back(std::string(quote->value()));
            while(cit = cit->next_sibling("cit"))
            {
                quote = cit->first_node("quote");
                gramGrp = quote->next_sibling("gramGrp");

                if(gramGrp != 0)
                {
                    gen = gramGrp->first_node("gen");
                    if(gen != 0) trans.push_back(std::string(quote->value()) + " {" + std::string(gen->value()) + "}");
                    else trans.push_back(std::string(quote->value()));
                } else trans.push_back(std::string(quote->value()));
            }
        }

        Entries.push_back(new Entry(orth, trans));
    }
}



















