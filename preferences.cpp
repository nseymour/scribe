#include "preferences.h"
#include "langwriter.h"

Preferences::Preferences()
{
    JsonSerializer::DeserializeFromFilepath(this, LangWriter::GetFullPath("DATA/preferences.json"));
}

void Preferences::Deserialize( Json::Value& root )
{
    LangPaths.clear();

    Json::Value paths = root.get("lang_paths", 0);

    for(int i = 0; i < paths.size(); i++)
    {
        LangPaths.push_back(LangWriter::GetFullPath(paths[i].asString()));
    }
}

void Preferences::Serialize(Json::Value &root)
{

}

