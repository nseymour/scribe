#ifndef DICTLOADER_H
#define DICTLOADER_H

#include <QObject>
#include <QThread>
#include "language.h"

class DictLoader : public QObject
{
    Q_OBJECT
    QThread workerThread;

public slots:
    void doWork(Language *lang, bool reverse = false);

signals:
    void enableDict();
};

#endif // DICTLOADER_H
