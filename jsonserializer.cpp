#include "jsonserializer.h"

bool JsonSerializer::Serialize( IJsonSerializable* pObj, std::string& output )
{
   if (pObj == NULL)
      return false;

   Json::Value serializeRoot;
   pObj->Serialize(serializeRoot);

   Json::StyledWriter writer;
   output = writer.write( serializeRoot );

   return true;
}

bool JsonSerializer::Deserialize( IJsonSerializable* pObj, std::string& input )
{
   if (pObj == NULL)
      return false;

   Json::Value deserializeRoot;
   Json::Reader reader;

   if ( !reader.parse(input, deserializeRoot) )
      return false;

   pObj->Deserialize(deserializeRoot);

   return true;
}

bool JsonSerializer::DeserializeFromFilepath(IJsonSerializable* pObj, std::string path)
{
    std::string line;
    std::string output;
    std::ifstream json(path);
    if(json.is_open())
    {
        while(std::getline(json, line))
        {
            output.append(line + "\n");
        }
        json.close();
    }
        else
    {
        return false;
    }

    return JsonSerializer::Deserialize(pObj, output);
}

bool JsonSerializer::SerializeToFilepath(IJsonSerializable *pObj, std::string path)
{
    std::string output;
    JsonSerializer::Serialize(pObj, output);

    std::ofstream outfile;
    outfile.open(path);
    outfile << output;
    outfile.close();

    return true;
}




