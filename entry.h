#ifndef ENTRY_H
#define ENTRY_H

#include <iostream>
#include <vector>

class Entry
{
public:
    Entry(std::string orth, std::vector<std::string> trans);

    std::string Orth;
    std::vector<std::string> Trans;
};

#endif // ENTRY_H
