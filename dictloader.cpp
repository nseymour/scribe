#include "dictloader.h"

void DictLoader::doWork(Language *lang, bool reverse) {
    lang->DictInit(reverse);
    emit enableDict();
}
