#-------------------------------------------------
#
# Project created by QtCreator 2017-02-26T00:41:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LangWriter
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        langwriter.cpp \
    language.cpp \
    jsonserializer.cpp \
    jsoncpp.cpp \
    editor.cpp \
    prompter.cpp \
    entry.cpp \
    reader.cpp \
    dictloader.cpp \
    preferences.cpp \
    study.cpp \
    flashcardmanager.cpp \
    flashcardedit.cpp

HEADERS  += langwriter.h \
    language.h \
    ijsonserializable.h \
    jsonserializer.h \
    editor.h \
    prompter.h \
    json/json-forwards.h \
    json/json.h \
    rapidxml/rapidxml.hpp \
    rapidxml/rapidxml_iterators.hpp \
    rapidxml/rapidxml_print.hpp \
    rapidxml/rapidxml_utils.hpp \
    entry.h \
    reader.h \
    dictloader.h \
    preferences.h \
    study.h \
    flashcardmanager.h \
    flashcardedit.h

FORMS    += langwriter.ui \
    editor.ui \
    reader.ui \
    study.ui \
    flashcardedit.ui

win32:RC_ICONS += "assets\\scribelogo_seaFfu_256px.ico"
