#ifndef PROMPTER_H
#define PROMPTER_H

#include <iostream>
#include <vector>
#include <random>
#include <cstdlib>
#include <ctime>
#include <chrono>
#include "language.h"
#include "jsonserializer.h"

class Prompter : public IJsonSerializable
{
public:
    std::string StringPrompt();

    virtual void Serialize( Json::Value& root );
    virtual void Deserialize( Json::Value& root);
    void JsonArrayToVector( Json::Value& array, std::vector<std::string> *vect );

    Prompter();

private:
    std::vector<std::string> Patterns;
    std::vector<std::string> QPhrases;
    std::vector<std::string> Adjectives;
    std::vector<std::string> Nouns;
    std::vector<std::string> Possessives;
    std::vector<std::string> Imperatives;
    std::vector<std::string> Prepositions;
};

#endif // PROMPTER_H
