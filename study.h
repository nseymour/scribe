#ifndef STUDY_H
#define STUDY_H

#include <QMainWindow>
#include <QString>
#include <iostream>
#include "language.h"

namespace Ui {
class Study;
}

class Study : public QMainWindow
{
    Q_OBJECT

public:
    explicit Study(Language *lang, QWidget *parent = 0);
    ~Study();

private slots:
    void on_btn_prev_clicked();

    void on_btn_next_clicked();

    void on_btn_flip_clicked();

private:
    Ui::Study *ui;
    Language *language;
    int index = 0;
    bool Front = true;
    void Refresh();
};

#endif // STUDY_H
