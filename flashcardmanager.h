#ifndef FLASHCARDMANAGER_H
#define FLASHCARDMANAGER_H

#include <iostream>
#include <vector>
#include "ijsonserializable.h"
#include "json/json.h"

class FlashcardManager : public IJsonSerializable
{
public:
    FlashcardManager();
    virtual void Serialize( Json::Value& root );
    virtual void Deserialize( Json::Value& root);

    class Flashcard
    {
    public:
        Flashcard(std::string front, std::string back) :  Front(front), Back(back) {}
        Flashcard() {}
        std::string Front = "";
        std::string Back = "";

        std::string asString()
        {
            return Front + " --- " + Back;
        }
    };

    std::vector<Flashcard*> Flashcards;
};

#endif // FLASHCARDMANAGER_H
