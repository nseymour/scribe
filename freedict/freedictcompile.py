#!/usr/bin/python3

import sys
import re
import io

filepath = sys.argv[1]
output = sys.argv[1]

print ("Reading file...")
freedictfile = open(filepath, "r", encoding="utf8")
freedict = freedictfile.read()
print ("File read.")

junk_list = [r"<teiHeader>(.*)<\/teiHeader>", r"<pron>(.*?)<\/pron>"]

print ("Beginning newline deletion")
freedict = re.sub("\n", '', freedict)

print ("Beginning junk deletion")
for r in junk_list:
    freedict = re.sub(r, '', freedict)

print ("Operations done! Writing to file.")
with open(output, 'w', encoding="utf8") as f:
    f.write(freedict)
print ("Done!")