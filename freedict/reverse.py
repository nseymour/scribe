#!/usr/bin/python3

import xml.etree.ElementTree as etree
import sys

filepath = sys.argv[1]
outpath = sys.argv[2]

reverse = {}

print("Parsing file...")
langxml = etree.parse(filepath).getroot()
text = langxml.find("text")
print("File parsed! Getting entries...")

#print("Got " + str(len(body)) + " entries. Beginning reversal.")
