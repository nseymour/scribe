#ifndef LANGUAGE_H
#define LANGUAGE_H

#include <iostream>
#include <fstream>
#include <vector>
#include "json/json.h"
#include "ijsonserializable.h"
#include "rapidxml/rapidxml.hpp"
#include "entry.h"
#include "flashcardmanager.h"
#include "jsonserializer.h"
#include <QMessageBox>
#include <Qstring>
#include <algorithm>

// Base Class
class Language : public IJsonSerializable
{
public:
    Language();
    virtual ~Language( void );
    virtual void Serialize( Json::Value& root );
    virtual void Deserialize( Json::Value& root);

    std::vector<std::string> Lookup(std::string word, bool onlyfull);
    void DictInit(bool reverse);

    std::string printName;
    std::string abbreviation;

    bool conjugation;

    Json::Value json;

    std::string DictPath;
    std::string DictReversePath;
    std::string Dict;

    std::string FlashcardPath;

    rapidxml::xml_document<> DictXML;

    std::vector<Entry*> Entries;

    FlashcardManager *FManager;
};

#endif // LANGUAGE_H
