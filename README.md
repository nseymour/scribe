# About

http://scribeapp.net

Scribe is a tool built by a language learner, for other language learners. It has two basic functionalities:

1. Writing

- Provides the user with rich, random writing prompts for when you just don't know what to write about

- Has a built in dictionary to allow you to look up words you don't know instantly!


2. Reading

- Load up any old text file you have on your computer and read away!

- Just double click the words that you do not know, and translation appear!


# Supported Languages:

Scribe was made to support only German, but also has limited Spanish and French functionality.